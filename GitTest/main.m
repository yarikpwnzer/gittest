//
//  main.m
//  GitTest
//
//  Created by user on 14.10.16.
//  Copyright © 2016 YAGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
