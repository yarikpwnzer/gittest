//
//  AppDelegate.h
//  GitTest
//
//  Created by user on 14.10.16.
//  Copyright © 2016 YAGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

